#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){

    ofSetFrameRate(30);
    ofBackground(0);
    
    osc.setupReceiver(true);
    ofAddListener(osc.onMessageReceived, this, &testApp::onMessageReceived);
    
}

//--------------------------------------------------------------
void testApp::update(){
    
    osc.update();

}

//--------------------------------------------------------------
void testApp::draw(){

    osc.drawReport();
    
}

//--------------------------------------------------------------
void testApp::onMessageReceived(ofxWeloveOscMessage &msg)
{
    string addr = msg.msg_adress;
    string value = msg.msg_str;
    
    
    string k = osc.checkKey(value);
    if(k != "nokey") {
        cout << "osc recived: " << k + ": " + value << "\n";
    }
    
    // /millumin/board/columnLaunched
    // /millumin/composition/cue
    // numero de columnas
    //cout << "osc recived: " << addr + ": " + value << "\n";
    
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}