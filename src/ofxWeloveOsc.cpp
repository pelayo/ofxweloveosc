//
//  ofxWeloveOsc.pp
//  emptyExample
//
//  Created by Pelayo M. on 13/03/13.
//
//

#include "ofxWeloveOsc.h"

//--------------------------------------------------------------
ofxWeloveOsc::ofxWeloveOsc()
:portSendingOSCTo(12000)
,hostSendingOSCTo("localhost")
,sendMsgCacheSize(20)
,portRecivingOSC(12345)
,recivedMsgCacheSize(30)
,consoleLogsTraffic(false)
{

	
}

//--------------------------------------------------------------
void ofxWeloveOsc::loadXmlSettings(string xmlfile) {
    
    XMLConfig.loadFile(xmlfile);
    portSendingOSCTo = XMLConfig.getValue("SETTINGS:OSCPORTSEND", portSendingOSCTo);
    hostSendingOSCTo = XMLConfig.getValue("SETTINGS:OSCHOSTSEND", hostSendingOSCTo);
    portRecivingOSC = XMLConfig.getValue("SETTINGS:OSCPORTRECIVE", portRecivingOSC);
    
}

//--------------------------------------------------------------
void ofxWeloveOsc::setupSender() {
    
    setupSender(portSendingOSCTo,hostSendingOSCTo);
    
}

//--------------------------------------------------------------
void ofxWeloveOsc::setupSender(int port, string host) {
    
    portSendingOSCTo = port;
    hostSendingOSCTo = host;
    sender.setup(hostSendingOSCTo, portSendingOSCTo);
    
    activeCachePos = 0;
    sendCacheLog.resize(sendMsgCacheSize);
    
    ofLogNotice() << "OSC sending messages to: " << hostSendingOSCTo << " on port: " << portSendingOSCTo;
    
}

//--------------------------------------------------------------
void ofxWeloveOsc::setupReceiver(bool loadXmlSettings) {
    
    setupReceiver(portRecivingOSC, loadXmlSettings);

}

//--------------------------------------------------------------
void ofxWeloveOsc::setupReceiver(int port, bool loadXmlSettings) {
    
    portRecivingOSC = port;
    receiver.setup(portRecivingOSC);
    ofLogNotice() << "Listening for osc messages on port: " << portRecivingOSC;
    
    msgCache.resize(recivedMsgCacheSize);
    
    if(loadXmlSettings) loadXmlControlData();
        
}

//--------------------------------------------------------------
void ofxWeloveOsc::sendMessage(string m, int val) {
    
    ofxOscMessage note;
    note.setAddress(m);
    note.addIntArg(val);
    sender.sendMessage( note );
    
    if(consoleLogsTraffic) ofLogNotice() << "Osc msg send: " << m << " with value: " << val;
    
    string logmsg = m + " " + ofToString(val);
    activeCachePos = (activeCachePos + 1) % sendMsgCacheSize;
    sendCacheLog[activeCachePos] = logmsg;

}

//--------------------------------------------------------------
void ofxWeloveOsc::sendMessage(ofxOscMessage msg) {
    
    sender.sendMessage( msg );

        string msgtext;
    
        for(int i = 0; i < msg.getNumArgs(); i++){
        if(msg.getArgType(i) == OFXOSC_TYPE_INT32){
            msgtext += ofToString(msg.getArgAsInt32(i)) + " ";
        }
        else if(msg.getArgType(i) == OFXOSC_TYPE_FLOAT){
            msgtext += ofToString(msg.getArgAsFloat(i)) + " ";
        }
        else if(msg.getArgType(i) == OFXOSC_TYPE_STRING){
            msgtext += msg.getArgAsString(i) + " ";
        }
        else{
           msgtext +=  "unknown ";
        }
        }
    
    if(consoleLogsTraffic) ofLogNotice() << "Osc msg send: " << msg.getAddress() << " with values:\n" << msgtext;
    
    string logmsg = msg.getAddress() + " " + msgtext;
    activeCachePos = (activeCachePos + 1) % sendMsgCacheSize;
    sendCacheLog[(sendMsgCacheSize-1)-activeCachePos] = logmsg;

}

//--------------------------------------------------------------
void ofxWeloveOsc::update() {
    
	// check for waiting messages
	while(receiver.hasWaitingMessages()){
        
        // get the next message
		ofxOscMessage msg;
		if (receiver.getNextMessage(&msg))
        {
         
            ofxWeloveOscMessage wlcmsg;
            wlcmsg.msg_adress = msg.getAddress();
            wlcmsg.msg_time = ofGetElapsedTimef();
            
            // TODO: Work with methods from the class.
            wlcmsg.oscMsg = msg;
            
            for(int i = 0; i < msg.getNumArgs(); i++){
				if(msg.getArgType(i) == OFXOSC_TYPE_INT32){
					wlcmsg.msg_str = ofToString(msg.getArgAsInt32(i)) + "%s";
				}
				else if(msg.getArgType(i) == OFXOSC_TYPE_FLOAT){
					wlcmsg.msg_str = ofToString(msg.getArgAsFloat(i)) + "%s";
				}
				else if(msg.getArgType(i) == OFXOSC_TYPE_STRING){
					wlcmsg.msg_str = msg.getArgAsString(i) + "%s";
				}
				else{
					wlcmsg.msg_str = "unknown";
				}
			}
            
            ofNotifyEvent(onMessageReceived, wlcmsg, this);
            if(consoleLogsTraffic) ofLogNotice() << "Osc msg recived: " << wlcmsg.msg_adress << " with value: " << wlcmsg.msg_str;
            
            // add msg cache
            msgCache[current_msg_string] = wlcmsg;
            current_msg_string = (current_msg_string + 1) % recivedMsgCacheSize;
            // clear the next line
            msgCache[current_msg_string].clearMsgInfo();
            
            }
            
	}
    
}

//--------------------------------------------------------------
void ofxWeloveOsc::drawSendReport(int posX, int posY)
{
    
    ofPushStyle();
    ofSetColor(255, 255, 255);
    
    string buf;
	buf = "Sending osc messages to " + ofToString(hostSendingOSCTo) + " on port: " + ofToString(portSendingOSCTo) + "\n\n";
	
	for(int i = 0; i < sendMsgCacheSize; i++){
        buf += sendCacheLog[(sendMsgCacheSize-1)-i] + "\n";
    }
    
    ofDrawBitmapString(buf, posX, posY);
    
    ofPopStyle();
    
}

//--------------------------------------------------------------
void ofxWeloveOsc::drawReciveReport()
{
    
    ofPushStyle();
    ofSetColor(255, 255, 255);

    string buf;
	buf = "listening for osc messages on port: " + ofToString(portRecivingOSC);
	ofDrawBitmapString(buf, 10, 20);
    
	for(int i = 0; i < recivedMsgCacheSize; i++){
        if(msgCache[current_msg_string].msg_time > 0) {
            ofDrawBitmapString(msgCache[current_msg_string].msg_str, 10, 40 + 15 * i);
        }
    }
    
    ofPopStyle();
    
}

//--------------------------------------------------------------
string ofxWeloveOsc::getReceivingPortAsString()
{
    return ofToString(portRecivingOSC);
};

//--------------------------------------------------------------
string ofxWeloveOsc::getSendingHostAsString()
{
    return ofToString(hostSendingOSCTo);
};

//--------------------------------------------------------------
string ofxWeloveOsc::getSendingPortAsString()
{
    return ofToString(portSendingOSCTo);
};

//--------------------------------------------------------------
void ofxWeloveOsc::toggleConsoleLogsTraffic(bool bIsActive) {
    
    consoleLogsTraffic = bIsActive;
    
}

//--------------------------------------------------------------
void ofxWeloveOsc::loadXmlControlData()
{
 
    XML.loadFile("xml/osc_config.xml");
    int xmlNodeActive = 0;
    int numDragTags = XML.getNumTags("CONFIG");
    //cout << ofToString(numDragTags) << endl;
    if(numDragTags > 0){
        XML.pushTag("CONFIG:control", numDragTags-1);
        int numPtTags = XML.getNumTags("control");
        //cout << ofToString(numPtTags) << endl;
        //int xmlTotalLines = numPtTags;
        if(numPtTags > 0){
            //int totalToRead = MIN(numPtTags, NUM_PTS);
            //cout << ofToString(totalToRead) << endl;
            listItems.resize(numPtTags);
            for(int i = 0; i < numPtTags; i++){
                
                string type = XML.getValue("control:type", "",i);
                string value = XML.getValue("control:value", "",i);
                string keypressed = XML.getValue("control:keypressed", "",i);
                
                ofxWeloveOscItem u;
                u.type = XML.getValue("control:type", "",i);
                u.value = XML.getValue("control:value", "",i);
                u.keypressed = XML.getValue("control:keypressed", "",i);
                
                listItems[i] = u;
                
            }
            
            cout << "Parametros cargados: " + ofToString(listItems.size()) << endl;
            
        }
    }
    
}

//--------------------------------------------------------------
string ofxWeloveOsc::checkKey(string _value) {
    
    // TODO: Convertir en teclas los string correctos. No valor final.
    for(int i = 0; i < listItems.size(); i++){
        if(listItems[i].value == _value) {
            return listItems[i].keypressed;
        }
    }
    return "nokey";
    
};
