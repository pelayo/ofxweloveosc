/*
 *  ofxWeloveOsc.h
 *
 *  Created by Pelayo Méndez on 13/10/13.
 *  No rights reserved.
 *
 *  Created with OpenFrameworks 0074
 *	addons needed: ofxOsc, ofxXmlSettings
 *
 */

#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxXmlSettings.h"

#define OSC_NUM_MSG_STRINGS 30

class ofxWeloveOscMessage {
  
    public:
    
        string msg_adress;
        string msg_str;
        float msg_time;
    
        ofxOscMessage oscMsg;
    
        void clearMsgInfo() {
        
            msg_adress = "";
            msg_str = "";
            msg_time = 0;
            
        }
    
};

class ofxWeloveOscItem {
    
public:
    
    string type;
    string value;
    string keypressed;
    
};

class ofxWeloveOsc {
    
    public:
    
        ofxWeloveOsc();
    
        void loadXmlSettings(string xmlfile);
    
        void setupSender();
        void setupSender(int port, string host);
    
        void setupReceiver(bool loadXmlSettings);
        void setupReceiver(int port, bool loadXmlSettings);
    
        void update();
        ofEvent<ofxWeloveOscMessage> onMessageReceived;
    
        void sendMessage(string m, int val);
        void sendMessage(ofxOscMessage msg);
    
        void drawReciveReport();
        void drawSendReport(int posX, int posY);
    
        string getSendingHostAsString();
        string getReceivingPortAsString();
        string getSendingPortAsString();
    
        // Utils + Keyboard [WIP] 
    
        void toggleConsoleLogsTraffic(bool bIsActive);
        string checkKey(string _value);
	
    private:

        ofxXmlSettings XMLConfig;
    
        // Sending.
    
        ofxOscSender sender;
        int activeCachePos;
        int sendMsgCacheSize;
        vector<string> sendCacheLog;
    
        int portSendingOSCTo;
        string hostSendingOSCTo;

        // Receiving.
    
        ofxOscReceiver receiver;
    
        int portRecivingOSC;
    
        int recivedMsgCacheSize;
        int current_msg_string;
        vector<ofxWeloveOscMessage> msgCache;
    
        // Utils
    
        bool consoleLogsTraffic;
    
        // Keyboard utils [WIP]
    
        ofxXmlSettings XML;
        void loadXmlControlData();
        vector<ofxWeloveOscItem> listItems;
    
};